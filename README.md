# ![binaire](images/binaire.png) Le codage numérique du texte

## ![binairelogo](images/binaire_logo.png) Définition(source Wikipedia)

<p style="color:red">Le codage des caractères est une convention qui permet, à travers un codage connu de tous, de transmettre de l’information textuelle, là où aucun support ne permet l’écriture scripturale.<br>
Cela consiste à représenter chaque caractère, par un motif visuel, un motif sonore, ou une séquence abstraite. Les techniques des ordinateurs reposent sur l’association d’un caractère à un nombre.<\p>

## ![binairelogo](images/binaire_logo.png) Un peu d'histoire sur ASCII : American Standard Code for Information Interchange

>Au commencement, chaque caractère était identifié par un code unique qui est un entier naturel et la correspondance entre le caractère et son code était appelée un ``Charset``.
>Le code n'étant pas utilisable tel quel par un ordinateur qui ne comprend que le binaire, il fallut donc représenter les codes par des octets, et cela fut appelé ``Encoding``.
>
>Le code ASCII est utilisé pour représenter du texte en informatique. ASCII signifie ``American Standart Code for Information Interchange``.


Le code ASCII (standardisé en 1963) a été conçu pour représenter des textes écrits en anglais, il n'y a donc pas d'accents, de tréma...


En ASCII on code 95 caractères: les 10 chiffres, les 26 lettres minuscules, les 26 lettres majuscules, 32 symboles ( @, <, >..), l'espace; et 33 symbloes de mise en page ( passage à la ligne, saut de page...), soit 128 caractères.

Il faut donc 7 bits, mais on code sur 1 octet, le premier bit étant toujours 0 et sert au contrôle de parité (pour éviter des erreurs).

Les caractères de numéro 0 à 31 et le 127 ne sont pas affichables ; ils correspondent à des commandes de contrôle de terminal informatique.

+ Le caractère numéro 127 est la commande pour effacer.
+ Les chiffres sont codés par les nombres de 48 à 57
+ Les lettres majuscules par les nombres de 65 à 90
+ Les minuscules par les nombres de 97 à 122
+ @ est codé par 64

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 1

Combien un texte de $`n`$ caractères occupe-t-il d'octets dans un fichier texte codé en ASCII?

## ![binairelogo](images/binaire_logo.png) La taille d'un texte en ASCII

![image](https://www.sciencebuddies.org/references/ascii-table.png)

Quelle est la taille (en octets) de la phrase: ``Quand 900 ans comme moi tu auras, moins en forme tu seras !`` (attention, il faut compter les espaces, et signes de ponctuation...)? 

Vérifiez en tapant cette phrase avec un éditeur de texte quelconque comme le bloc-notes de Windows, TextEditsous OSX ou encore kwrite, geany sous Linux. Il suffit d’écrire le texte, puis de l’enregistrer en tant que texte brut (le plus souvent avec une extension .txt) et ensuite de vérifier la taille en octets du fichier obtenu (ce qui peut se faire en cliquant d’abord avec le bouton droit sur l’icône du fichier puis sur ``Propriétés``). 

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 2

|Texte|` `|` `|Nombre de caractères trouvés|
|:--:|:--:|:--:|:--:|
|Ce n’est pas parce que tu parles que tu es intelligent.|+|+|60|
|N’essaie pas ! Fais-le, ou ne le fais pas ! Il n’y a pas d’essai.|+|+|65|
|Personne par la guerre ne devient grand.|+|+|40|
|Qui est le plus fou des deux, le fou ou le fou qui le suit ?|+|+|55|

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 3

1. À l’aide de la table ASCII, coder en binaire la phrase suivante: ``This is not going to go the way you think!``.

```python

def deciBin8(decimal) :
    binaire = ''
    while decimal != 0 :
        binaire = f'{decimal % 2}{binaire}'
        decimal = decimal // 2
    while len(binaire) < 8 : 
        binaire = '0'+binaire
    
    return binaire

def chaineASCII(chaine) :
    
    codeBinaireChaine = ''    
    for caractere in chaine :
        codeBinaireChaine += deciBin8(ord(caractere))
        
    return codeBinaireChaine

```

2. Retrouve la phrase qui a été codé : ``0100100100100000011000010110110100100000011110010110111101110101011100100010000001100110011000010111010001101000011001010111001000101110``

```python

def decodeASCII(codeBin) :
    
    chaine = ''
    
    for i in range(0, len(codeBin), 8) :
        chaine += chr(int(codeBin[i:i+8], 2))
        
    return chaine
```

3. Peut-on coder en binaire la phrase à l’aide de la table ASCII ? : ``L’attachement, mène à la jalousie. À l’ombre de la convoitise il grandit.``

## ![binairelogo](images/binaire_logo.png) Je veux écrire avec des accents!

Outre l’anglais, le jeu de caractères du code ASCII permet d’écrire des textes techniques de toute langue utilisant l’alphabet latin, si on accepte d’ommettre les caractères accentués.

Cependant de telles ommissions, ne sont pas acceptables pour des textes de nature littéraire. C’est pourquoi de nombreuses extensions du code ASCII sont très vite apparues.

Parmi ces extensions citons la famille des codes ISO 8859.

Les codes ISO 8859 sont au nombre de 16 et désignés par ISO 8859-n où n est un entier compris entre 1 et 16.

Tous ces codes définissent chacun un jeu de 224 caractères codés de 0 à 127 et de 160 à 255 (les codes de 128 à 159 sont inutilisés). Tous ces caractères peuvent et sont représentés sur un octet.

Les caractères de codes compris entre 0 et 127 sont ceux du code ASCII. Les codes ISO 8859 se distinguent par le jeu des caractères de 160 à 255.

Pour avoir les caractères accentués du français il faut utiliser l’ISO 8859-1 (parfois appelé LATIN-1) ou l’ISO 8859-15 (parfois appelé LATIN-15) qui ne se distingue du précédent que par le remplacement de quelques caractères dont le ¤ par €. Avec ISO 8859-1 (ou ISO 8859-15), le é est codé 233.

![ISO 8859](https://www.alsacreations.com/xmedia/doc/original/latin1-table.png)

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 4

Quel est le code binaire du caractère ``È`` de la table ISO 8859-1? 


## ![binairelogo](images/binaire_logo.png) Et si j'échange du texte avec mon ami étrangé ?

Les diverses extensions du code ASCII, dont les ISO 8859, présentent l’inconvénient d’être incompatibles entre elles, et le plus souvent d’être spécialisées pour un jeu de caractères lié à une langue. Comment alors coder dans un même document des textes rédigés avec des alphabets aussi divers que les alphabets latin, cyrillique, grec, arabe, ... ?

C’est pourquoi Unicode a vu le jour au début des années 1990.

Ce standard est lié à la norme ISO/CEI 10646 qui décrit une table de caractères équivalente. La dernière version, Unicode 12.0, a été publiée en mars 2019.

Le standard Unicode est constitué d'un répertoire de 137 929 caractères, couvrant une centaine d’écritures, d'un ensemble de tableaux de codes pour référence visuelle, d'une méthode de codage et de plusieurs codages de caractères standard, d'une énumération des propriétés de caractère (lettres majuscules, minuscules, APL, symboles, ponctuation, etc.) d'un ensemble de fichiers de référence des données informatiques, et d'un certain nombre d'éléments liés, tels que des règles de normalisation, de décomposition, de tri, de rendu et d'ordre d'affichage bidirectionnel (pour l'affichage correct de texte contenant à la fois des caractères d'écritures de droite à gauche, comme l'arabe et l'hébreu, et de gauche à droite).

Unicode se contente de recenser, nommer les caractères et leur attribuer un numéro. Mais il ne dit pas comment ils doivent être codés en informatique : [UNICODE](https://unicode-table.com/fr/)

Plusieurs codages des caractères Unicode existent :

+ UTF-32 qui code chaque caractère sur 32 bits (soit quatre octets)
+ UTF-16 qui code chaque caractère sur 16 ou 32 bits (soit deux ou quatre octets)
+ UTF-8 qui code chaque caractère sur 8, 16, 24 ou 32 bits (soit un, deux, trois ou quatre octets).

Le plus couramment utilisé, notamment pour les pages Web, est UTF-8.

> Unicode est un standard permettant de représenter les symboles d’une grande partie des alphabets connus, plus d’autres utilisés dans différentes branches (mathématique, ingénierie, etc.).
>
>À chaque symbole Unicode est associé un entier, appelé en anglais son code point, dans l’intervalle de 0x0 à 0x10FFFF (0x indique un nombre hexadécimal). 
>Les caractères Unicode sont souvent représentés par U+ suivi du code point, par exemple U+0058 est le caractère ‘X’ de l’alphabet latin.
>
>Unicode est subdivisé en dix-sept plans (planes en anglais), de 216 code points chacun, ce qui fait un total de 1.112.114 codes possibles (en ce moment, environ 10% de ces codes sont utilisés). Parmi les plans on distingue le Basic Multilingual Plane (BMP), contenant les symboles les plus courants et dont les code points vont de U+0000 à U+FFFF, le Supplementary Multilingual Plane, allant de U+10000 à U+1FFFF, et le Supplementary Ideographic Plane, allant de U_20000 à U+2FFFF. Les symboles ASCII, plus précisément ceux du jeu ISO-8859-1, font partie du Basic Multilingual Plane et ont code points de U+0000 à U+00FF.
>
>UTF-16 est un standard de codage qui permet de représenter tous les code points de Unicode.

## ![binairelogo](images/binaire_logo.png) UTF-8

UTF-8 (UCS transformation format 8 bits) est un format de longueur variable, défini pour les caractères Unicode. Chaque caractère est codé sur une suite de un à quatre octets. UTF-8 a été conçu pour assurer une bonne compatibilité avec les logiciels prévus pour traiter des caractères d’un seul octet. Les protocoles de communication d’Internet échangeant du texte doivent supporter UTF-8.

Unicode attribue un numéro à chaque caractère. Les caractères de numéro 0 à 127 sont codés sur un octet dont le bit de poids fort est toujours nul. Les caractères de numéro supérieur à 127 sont codés sur plusieurs octets. Dans ce cas, les bits de poids fort du premier octet forment une suite de 1 de longueur égale au nombre d’octets utilisés pour coder le caractère, les octets suivants ayant 10 comme bits de poids fort.

Ce principe pourrait être étendu jusqu’à six octets pour un caractère, mais UTF-8 pose la limite à quatre. Ce principe permet également d’utiliser plus d’octets que nécessaire pour coder un caractère, mais UTF-8 l’interdit.

**Algorithme :**

On repère dans la colonne de gauche à quelle plage correspond le code point du symbole que l’on souhaite encoder.
On écrit le code point en binaire sur autant de bits qu’indiqué dans la colonne suivante, éventuellement en ajoutant des 0 à gauche.
On remplace les ‘x’ des colonnes suivantes par les bits calculés au point précédent, du plus significatif au moins significatif.
On remarque que:

+ Les code points dans la plage U+0000 – U+00FF sont encodés par les propre code point sur un octet. Ce sont les caractères du jeu ASCII 7, ce qui garantit la compatibilité en arrière de UTF-8 : tout fichier ASCII-7 valide est aussi un fichier UTF-8 valide contenant les mêmes symboles, et inversement tout fichier UTF-8 ne contenant que des symboles dans cette plage est un fichier ASCII valide.

+ Les octets qui commencent par 1 font partie d’un symbole encodé sur plusieurs octets.

+ Les octets qui commencent par 10 sont les octets du milieu d’un symbole encodé sur plusieurs octets

+ Le nombre de 1 précédent le 0 d’un octet ne commençant ni par 0 ni par 10 est égal au nombre d’octets sur lesquels est encodé le symbole UTF-8 contenant l’octet.

Les trois dernières propriétés font de UTF-8 un code instantané: tout octet provenant du milieu d’un flux UTF-8 peut immédiatement être reconnu comme étant le début ou non d’un symbole Unicode.

|Code point|Représentation binaire|Signification|
|:--:|:--:|:--:|
|U+0000 – U+007F|0xxxxxxx|1 octet codant 1 à 7 bits|
|U+0080 – U+07FF|110xxxxx 10xxxxxx|2 octets codant 8 à 11 bits|
|U+0800 – U+FFFF|1110xxxx 10xxxxxx 10xxxxxx|3 octets codant 12 à 16 bits|
|U+10000 – U+1FFFFF|11110xxx 10xxxxxx 10xxxxxx 10xxxxxx|4 octets codant 17 à 21 bits|

### Exemples : 

|Caractère|Commentaire|Hex|Décimal|Binaire|Binaire UTF-8|
|:--:|:--:|:--:|:--:|:--:|:--:|
|A|\ |x41|65|1000001(7bits)|01000001|
|Ú|\ |xDA|0218|11011010(8bits)|11000011 10011010|
|€|(Monnaie) signe Euro|x20AC|8364|10000010101100(14bits)|11100010 10000010 10101100|

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 5

Syllabe chérokie kwou est un caractére de point code unicode U+13CA.

+ Donner le code UTF-8 du caractère chérokie kwou en hexadécimal.
+ À l'aide du bloc-notes, créez le fichier vide kwou.txt.
+ À l'aide du logiciel [HXD](https://mh-nexus.de/en/downloads.php?product=HxD20) éditer le fichier kwou.txt et transférer le code UTF-8 du caractère chérokie kwou en hexadécimal.
+ Ouvrir le fichier avec le bloc-notes pour voir le caractère chérokie kwou.
+ Si on ouvre le fichier kwou.txt avec la table ISO 8859-1, que va-t-on voir s'afficher?

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 6

+ Encoder le code point unicode U+1F911 en UTF-8.
+ Vérifier votre code [ici](https://mothereff.in/utf-8).

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 7

Écrire la fonction ``unicode_utf8(code_point)`` en python.


```python
def unicode_utf8(code_point):
    '''
    La fonction unicode_utf8 donne le code utf-8 d'un code point unicode.
    
    @Paramètre : code_point est une variable qui référence un objet de type <class str> 
        représentant un code point unicode.
    @return : Un objet de type <class list> représentant le code utf-8 de code_point.
            
    @Exemple : 
        >>> unicode_utf8(263A)
        ['E2', '98', 'BA']  
    '''
```

## ![python_titre_bleu](images/Python_titre_bleu.png) **Le codage du texte et Python**

La table de caractères [unicode](https://unicode-table.com/fr/#control-character) nous donne les informations suivantes :

![signeProduit](images/produit.png)

## ![binaire_titre](images/binaire_logo_titre.png) Exercice 8

Écrire la fonction ``tableMultiplication(entier, dimension)``


```python
def tableMultiplication(entier, dimension):
    '''
    La fonction tableMultiplication affiche la table de multiplication de 'entier' de 0 à 'dimension'.
    
    @Paramètre : entier est une variable qui référence un objet de type <class int>.
    @Paramètre : dimension est une variable qui référence un objet de type <class int>.
            
    @Exemples : 
        >>> tableMultiplication(5, 10)
        0 × 5 = 0
        1 × 5 = 5
        2 × 5 = 10
        3 × 5 = 15
        4 × 5 = 20
        5 × 5 = 25
        6 × 5 = 30
        7 × 5 = 35
        8 × 5 = 40
        9 × 5 = 45
        10 × 5 = 50    
    '''
```

#### ![python_titre](images/python_titre.png) *À observer*


```python
ord('\u00D7')
```




    215




```python
chr(215)
```




    '×'



# ![projet](images/projet.png)  **Tkinter et les Emojis**


```python
u = ord('😬')
print(u)
```

    128556
    


```python
chr(u)
```




    '😬'



**Il est pourtant impossible d'afficher cet imoji, directement avec son code point Unicode ou son codage utf-8, dans l'interface Tkinter de conversion Binaire en décimal !**

![thonnyEmoji](images/thonnyEmoji.PNG)

### ![python_dossier](images/python_dossier.png) **L'UTF-16 dans les environnements Windows.**

UTF-16 est un standard de codage qui permet de représenter tous les codes points de Unicode. Les codes points du BMP sont représentés sur 2 octets, comme dans UCS-2, les codes points des plans supplémentaires sont représentés sur 4 octets.

Afin de permettre un décodage unique, une partie du BMP, la plage allant de U+D800 à U+DFFF, est réservée pour ce que l’on appelle codes subrogés. Ces codes ne représentent aucun vrai symbole : ils sont utilisés pour encoder les code points des symboles des plans supplémentaires.

**La présence des codes subrogés fait de UTF-16 un code instantané, en ce sens que n’importe quelle paire d’octets au milieu d’un flux UTF-16 peut être immédiatement reconnue comme étant un symbole du BMP, ou bien un code subrogé (haut ou bas) faisant partie d’un symbole encodé sur 4 octets.**

Étant donné un code point Unicode, son encodage UTF-16 est encodé sur 2 ou 4 octets comme suit:

- Si le code point appartient au BMP, il est écrit tel quel sur 2 octets. Ceci inclut tous les code points de ``U+0000`` à ``U+D7FF`` et de ``U+E000`` à ``U+FFFF``, mais exclut les codes subrogés de ``U+D800`` à ``U+DFFF`` qui sont réservés pour un usage interne.

- Si le code point n’appartient pas au BMP, sont code est compris entre ``U+10000`` et ``U+10FFFF``. L’algorithme procède ainsi:

    + On soustrait $10000_{16}$ du code point, obtenant un code compris entre $0_{16}$ et $FFFFF_{16}$.
    + Le résultat de l’opération précédente tient sur 20 bits. On coupe les 20 bits en deux blocs de 10 bits chacun, appelés partie haute (les bits plus significatifs) et partie basse (les bits moins significatifs).
    + On ajoute les bits haut à $D800_{16}$, en obtenant ainsi un code subrogé compris entre $D800_{16}$ et $DBFF_{16}$, appelé subrogé de tête.
    + On ajoute les bits bas à $DC00_{16}$, en obtenant ainsi un code subrogé compris entre $DC00_{16}$ et $DFFF_{16}$, appelé subrogé de queue.
    + Le symbole Unicode est encodé par le subrogé de tête écrit sur 2 octets, suivi par le subrogé de queue écrit sur 2 octets.

### ![python_dossier](images/python_dossier.png) **Application de l'algorithme**

'😬' a pour code point unicode $128556_{10} = 1F62C_{16}$

Déterminer le subrogé de tête et le subrogé de queue de ce symbole unicode.

### ![python_dossier](images/python_dossier.png) **Tkinter et les Emojis**

On cherche à construire une application qui permet d'afficher un Emoji à partir de son code point unicode.

![emojiIterface](images/emojiInterface.PNG)

### **Sturcture du projet à réaliser dans un dossier nommé emoji :**

![emojiIterface](images/stuctureEmoji.PNG)

#### ![python_titre](images/python_titre.png) *À observer*


```python
>>> hexa = 0xAB12
```


```python
>>> print(hexa)
```

    43794
    


```python
>>> binary = 0b10110110
```


```python
print(binary)
```

    182
    

## ![binairelogo](images/binaire_logo.png) Rédaction de la fonction ``unicode_utf16``


```python
def unicode_utf16(unicode):
    
    '''
    La fonction unicode_utf16 permet de calculer le subrogé de tête et le subrogé de queue d'un symbole unicode
    à partir de son code point unicode si ce dernier n'appartient pas au BMP et n'a pas un code subrogé résersé.
    
    @param::unicode est une variable qui référence un objet de type <class 'int'> représentant un code point unicode.
    @return::Un objet de type <class 'list'> de deux éléments de type <class 'int'> ---> [subrogé de tête, subrogé de queue]
    
    @exemples::
    >>> unicode_utf16(unicode)
    
    '''
```

## ![binairelogo](images/binaire_logo.png) Interface tkinter


```python
import PIL.Image
import PIL.ImageTk
import tkinter
from tkinter import ttk
from tkinter.font import *

import sys
sys.path.append('.............')

from ...........................

image = PIL.Image.open("...............")

interface = tkinter.Tk()
interface.geometry("...................")
interface.iconbitmap("......................")
interface.title("....................")

photo = PIL.ImageTk.PhotoImage(image.resize((...........,..............)))

maFonte = Font(family = "Times",
               size = 15,
               weight = BOLD)

maFonte_unicode = Font(family = "Times",
               size = 30,
               weight = BOLD)

canvas = tkinter.Canvas(interface,
                       width = 400,
                       height = 400,
                       bg = "#FFFFFF")

canvas.create_image(200, 200, image = photo)

canvas.create_text(200, 30, text='Code point unicode | Emoji', font = maFonte, fill = '#000000')

unicode = tkinter.Entry(canvas, font = maFonte, justify = tkinter.CENTER, fg = '#006600')

decode_unicode = tkinter.StringVar()

emoji = ttk.Entry(canvas,
                font = maFonte_unicode,
                justify = tkinter.CENTER,
                state = tkinter.DISABLED,
                textvariable = decode_unicode)

dessiner = ttk.Button(canvas,
               text = 'Emo',
               width = 4,
               command = unicode_utf16(unicode.get()))

emoji.place(relx=0.5, anchor = 'center', y=300, width=50)
dessiner.place(relx=0.5, anchor = 'center', y=180)
unicode.place(relx=0.5, anchor = tkinter.CENTER, y=80, width=150)
canvas.pack(expand=tkinter.YES, fill=tkinter.BOTH)
interface.mainloop()
```
